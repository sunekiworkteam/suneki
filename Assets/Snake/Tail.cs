﻿using UnityEngine;
//using UnityEditor;
using System.Collections;

public class Tail : MonoBehaviour {

	enum _Curves
	{
		posX,
		posY,
		posZ,
		rotX,
		rotY,
		rotZ
	};

    private float refreshTimer;
    public Animation anim;
	public AnimationClip Clip;
    public Transform prefab;
    public int size;
	private AnimationCurve[] Curves;

    public void launchTailChunk()
    {
        size += 1;
        Instantiate(prefab);
    }

	// Use this for initialization
	void Start () {
        refreshTimer = 0f;
        size = 0;
		Clip = new AnimationClip();
        Clip.name = "Test";
        Clip.wrapMode = WrapMode.Once;
        Curves = new AnimationCurve[7];
        Curves[0] = new AnimationCurve(new Keyframe(0, transform.localPosition.x));
        Curves[1] = new AnimationCurve(new Keyframe(0, transform.localPosition.y));
        Curves[2] = new AnimationCurve(new Keyframe(0, transform.localPosition.z));
        Curves[3] = new AnimationCurve(new Keyframe(0, transform.localRotation.x));
        Curves[4] = new AnimationCurve(new Keyframe(0, transform.localRotation.y));
        Curves[5] = new AnimationCurve(new Keyframe(0, transform.localRotation.z));
        Curves[6] = new AnimationCurve(new Keyframe(0, transform.localRotation.w));
    }
	
	// Update is called once per frame
	void FixedUpdate () {
        if (refreshTimer >= 0.3f)
        {
            refreshTimer -= 0.3f;
            Curves[0].AddKey(Time.realtimeSinceStartup, transform.localPosition.x);
            Curves[1].AddKey(Time.realtimeSinceStartup, transform.localPosition.y);
            Curves[2].AddKey(Time.realtimeSinceStartup, transform.localPosition.z);
            Curves[3].AddKey(Time.realtimeSinceStartup, transform.localRotation.x);
            Curves[4].AddKey(Time.realtimeSinceStartup, transform.localRotation.y);
            Curves[5].AddKey(Time.realtimeSinceStartup, transform.localRotation.z);
            Curves[6].AddKey(Time.realtimeSinceStartup, transform.localRotation.w);
            Clip.SetCurve("", typeof(Transform), "localPosition.x", Curves[0]);
            Clip.SetCurve("", typeof(Transform), "localPosition.y", Curves[1]);
            Clip.SetCurve("", typeof(Transform), "localPosition.z", Curves[2]);
            Clip.SetCurve("", typeof(Transform), "localRotation.x", Curves[3]);
            Clip.SetCurve("", typeof(Transform), "localRotation.y", Curves[4]);
            Clip.SetCurve("", typeof(Transform), "localRotation.z", Curves[5]);
            Clip.SetCurve("", typeof(Transform), "localRotation.w", Curves[6]);
        }
        refreshTimer += Time.deltaTime;
    }
}
