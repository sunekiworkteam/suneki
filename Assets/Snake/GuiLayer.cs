﻿using UnityEngine;
using System.Collections;

public class GuiLayer : MonoBehaviour
{
    EntityGenerator entities;
    public delegate void RestartAction();
    public static event RestartAction OnRestart;
    // Use this for initialization
	void Start ()
    {
        entities = GameObject.Find("Box").GetComponent<EntityGenerator>();
	}
	
	// Update is called once per frame
	void Update ()
    {
	
	}

    void OnGUI()
    {
        if (!entities.player.isAlive)
        {
            GUI.Label(new Rect(25, 25, 100, 30), "YOu ArE DeAD");
            if (GUI.Button(new Rect(Screen.width / 2 - 50, 5, 100, 30), "Restart"))
            {
                if (OnRestart != null)
                    OnRestart();
            }
			if (GUI.Button (new Rect(Screen.width / 2 - 50, 40, 100, 30), "Quit"))
			{
				Application.Quit();
			}
        }
        GUI.Label(new Rect(Screen.width - 80, 25, 100, 30),
            "Score: " + entities.player.score.ToString());
    }
}
