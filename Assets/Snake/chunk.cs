﻿using UnityEngine;
using System.Collections;

public class chunk : MonoBehaviour
{

    private GameObject go;
    private Tail script;
    private float timer;

    // Use this for initialization
    void Start()
    {
        go = GameObject.Find("Sphere");
        script = go.GetComponent<Tail>();
        timer = 0f;
    }

    // Update is called once per frame
    void FixedUpdate()
    {
        timer += Time.deltaTime;
//        if (timer >= 0.2f)
        if (Time.frameCount == 40)
        {
            script.launchTailChunk();
            timer -= 0.2f;
        }
    }
}
