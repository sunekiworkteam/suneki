﻿using UnityEngine;
//using UnityEditor;
using System.Collections;

public class AnimTruc : MonoBehaviour
{
    private GameObject snake;
    private EntityGenerator generator;
    private Tail script;
    private AnimationClip clip;
    private Quaternion localRot;
    private float timer;

    void Start()
    {
        snake = GameObject.Find("Snake(Clone)");
        generator = GameObject.Find("Box").GetComponent<EntityGenerator>();
        script = snake.GetComponent<Tail>();
        animation.wrapMode = WrapMode.Once;
        clip = script.Clip;
        timer = Time.realtimeSinceStartup - 0.3f * script.size;
        animation.AddClip(clip, clip.name);
//        AnimationUtility.GetAnimationClips(this.gameObject)[0] = clip;
        animation["Test"].time = 0f;
        localRot = transform.rotation;
        if (timer < 0f)
            animation["Test"].speed = 0f;
        animation.Play("Test");
    }

    void Update()
    {
        if (!generator.player.isAlive)
        {
            animation["Test"].speed = 0f;            
            return;
        }
        if (timer < 0f)
        {
            timer += Time.deltaTime;
            return;
        }
        animation["Test"].speed = 1f;
        animation["Test"].time = timer;
        animation.Play("Test");
        timer += Time.deltaTime;
    }

    void LateUpdate()
    {
        transform.localRotation *= localRot;
    }
}
