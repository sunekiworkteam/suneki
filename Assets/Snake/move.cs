﻿using UnityEngine;
using System.Collections;

public class move : MonoBehaviour
{
    EntityGenerator generator;
    private float moveSpeed = 10f;
    public float maxSpeed = 35;
    public float MinTurn = 10f;
    public float MaxTurn = 60f;
    public float Smooth = 2;
    private float RotSpeed;

    void Start()
    {
        GameObject Box = GameObject.Find("Box");
        generator = Box.GetComponent<EntityGenerator>();
        moveSpeed = 10f;
        RotSpeed = MinTurn;
    }

    void Update()
    {
        MoveForward();
        turn();
    }

    void MoveForward()
    {
        if (moveSpeed < maxSpeed)
            moveSpeed += 0.1f;
        if (generator.player.isAlive)
            transform.Translate(Vector3.forward * moveSpeed * Time.deltaTime);
    }

    private void turn()
    {
        bool onTurn = false;

        if (Input.GetKey(KeyCode.LeftArrow))
            onTurn = AplyRotation(-Vector3.up);
        if (Input.GetKey(KeyCode.RightArrow))
            onTurn = AplyRotation(Vector3.up);
        if (Input.GetKey(KeyCode.UpArrow))
            onTurn = AplyRotation(Vector3.right);
        if (Input.GetKey(KeyCode.DownArrow))
            onTurn = AplyRotation(-Vector3.right);
        if (!onTurn && RotSpeed > MinTurn)
                RotSpeed -= 10;
        if (onTurn && RotSpeed < MaxTurn)
            RotSpeed += Smooth;
    }

    private bool AplyRotation(Vector3 axis)
    {
        transform.Rotate(axis, RotSpeed * Time.deltaTime);
        return true;
    }
}
