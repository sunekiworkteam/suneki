﻿using UnityEngine;
using System.Collections;

public class Collisions : MonoBehaviour
{
    Collider[] HitArray;
    EntityGenerator generator;
	Tail scriptTail;

    void Start()
    {
        GameObject Box = GameObject.Find("Box");
        generator = Box.GetComponent<EntityGenerator>();
		scriptTail = this.GetComponent<Tail>();
	}

    void OnTriggerEnter(Collider col)
    {
        if (col.gameObject.name.Equals("Food(Clone)"))
        {
            Destroy(col.gameObject);
            generator.Food.CreatedNb--;
            if (generator.Food.Max < 6)
                generator.Food.Max++;
            generator.player.Eat();
			scriptTail.launchTailChunk();
        }
        else
            generator.player.Die();
    }
}
