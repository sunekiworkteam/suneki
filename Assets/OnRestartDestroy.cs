﻿using UnityEngine;
using System.Collections;

public class OnRestartDestroy : MonoBehaviour {

    void OnEnable()
    {
        GuiLayer.OnRestart += DestroyEntity;
    }

    void OnDisable()
    {
        GuiLayer.OnRestart -= DestroyEntity;
    }

    void DestroyEntity()
    {
        Destroy(this.gameObject);
    }
}
