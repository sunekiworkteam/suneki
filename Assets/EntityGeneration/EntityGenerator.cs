﻿using UnityEngine;
using System.Collections;

public class EntityGenerator : MonoBehaviour {

    public Transform    BarrierPrefab;
    public int          BarrierMax = 1000;
    public Entity       Barriers;

    public Transform    FoodPrefab;
    public Entity       Food;

    public Transform    playerPrefab;
    public Player       player;

    public BoxBounds   Box;

    void Awake()
    {
        Box = new BoxBounds(this.gameObject);
        Barriers = new Entity(BarrierMax, BarrierPrefab);
        Food = new Entity(1, FoodPrefab);
        player = new Player(1, playerPrefab);
    }
	void Start ()
    {
        Entity.Generate(player, Box);
        Entity.Generate(Barriers, Box);
	}
	
	void Update ()
    {
        Entity.Generate(Food, Box);
	}
}
