﻿using UnityEngine;
using System.Collections;

public class Restart : MonoBehaviour {

    private EntityGenerator script;

    void Start()
    {
        if (!(script = this.GetComponent<EntityGenerator>()))
            Debug.Log("EntityGenerator not found");
    }

    void OnEnable()
    {
        GuiLayer.OnRestart += RestartEntities;
    }

    void OnDisable()
    {
        GuiLayer.OnRestart -= RestartEntities;
    }

    void RestartEntities()
    {
        script.player.Reset();
        script.Food.Reset();
        Entity.Generate(script.player, script.Box);
    }
}
