﻿using UnityEngine;
using System.Collections;

public class BoxBounds
{
    private Vector3 maxRange;
    private Vector3 minRange;
    public Vector3 MaxRange { get { return maxRange; } }
    public Vector3 MinRange { get { return minRange; } }
    public Bounds BoxBound { get; private set; }

    public BoxBounds(GameObject Area)
    {
        BoxBound = GetBounds(Area);
        maxRange.x = BoxBound.center.x + BoxBound.extents.x - 10f;
        maxRange.y = BoxBound.center.y + BoxBound.extents.y - 10f;
        maxRange.z = BoxBound.center.z + BoxBound.extents.z - 10f;
        minRange.x = BoxBound.center.x - BoxBound.extents.x + 10f;
        minRange.y = BoxBound.center.y - BoxBound.extents.y + 10f;
        minRange.z = BoxBound.center.z - BoxBound.extents.z + 10f;
    }

    public static Bounds GetBounds(GameObject Obj)
    {
        Bounds bounds;
        Renderer childRender;

        bounds = GetRenderBounds(Obj);
        if (bounds.extents.x == 0f)
        {
            bounds = new Bounds(Obj.transform.position, Vector3.zero);
            foreach (Transform child in Obj.transform)
            {
                childRender = child.GetComponent<Renderer>();
                if (childRender)
                    bounds.Encapsulate(childRender.bounds);
                else
                    bounds.Encapsulate(GetBounds(child.gameObject));
            }
        }
        return bounds;
    }

    private static Bounds GetRenderBounds(GameObject objeto)
    {
        Bounds bounds = new Bounds(Vector3.zero, Vector3.zero);
        Renderer render = objeto.GetComponent<Renderer>();
        if (render != null)
            return render.bounds;
        return bounds;
    }
}
