﻿using UnityEngine;
using System.Collections;

public class Entity
{
    public int          Max;
    public int          CreatedNb;
    public Transform    Prefab;

    public Entity(int max, Transform Prefab)
    {
        CreatedNb = 0;
        Max = max;
        this.Prefab = Prefab;
    }

    public static void Generate(Entity entity, BoxBounds Box)
    {
        int MaxGenerations = 0;

        while (entity.CreatedNb < entity.Max && MaxGenerations < 3000)
        {
            Vector3 Spawn = GetRandSpawn(Box);
            if (!Physics.CheckSphere(Spawn, 10f))
            {
                MonoBehaviour.Instantiate(entity.Prefab, Spawn, Quaternion.identity);
                entity.CreatedNb++;
            }
            MaxGenerations++;
        }
    }

    private static Vector3 GetRandSpawn(BoxBounds Box)
    {
        Vector3 Spawn;

        if (Box == null)
            return Vector3.zero;
        Spawn = new Vector3(Random.Range(Box.MinRange.x, Box.MaxRange.x),
            Random.Range(Box.MinRange.y, Box.MaxRange.y),
            Random.Range(Box.MinRange.z, Box.MaxRange.z));
        return Spawn;
    }

    public virtual void Reset()
    {
        Max = 1;
        CreatedNb = 0;
    }
}
