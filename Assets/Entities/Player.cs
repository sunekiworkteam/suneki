﻿using UnityEngine;
using System.Collections;

public class Player : Entity
{
    public bool isAlive { get; private set; }
    public int  tailSize { get; private set; }
    private int initialSize;
    public int  score { get; private set; }
    private int growth;

    public Player(int max, Transform prefab) : base(max, prefab)
    {
        score = 0;
        isAlive = true;
        tailSize = growth;
        initialSize = tailSize;
        growth = 3;
    }

    public Player(int max, Transform prefab, int tailSize)
        : base(max, prefab)
    {
        score = 0;
        isAlive = true;
        this.tailSize = tailSize;
        growth = 3;
    }

    public void Eat()
    {
        tailSize += growth;
        score += 1;
        Debug.Log(score);
    }

    public void Die()
    {
        isAlive = false;
        this.Prefab.transform.Translate(Vector3.zero);
        Debug.Log("Vous etes mort !");
    }

    public override void Reset()
    {
        CreatedNb = 0;
        isAlive = true;
        tailSize = initialSize;
        score = 0;
    }
}
