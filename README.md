# README #

* * *
### Presentation ###

Suneki is a 3D game developed with Unity3D and using C# scripting.

This is currently a simple clone of the game "Snake", adapted in 3D world.

Actual version is alpha 0.1.

The gameplay differs little from the original: 

* Eat blocks to grow up and score
* Don't touch a wall or the snake tail
* Random blocks bar the way and set up difficulty (Maybe a bit too much)
* * *
### Setup ###
Download project and run with Unity3D.
* * *
### Incoming ###
* Gameplay adjustments
* A real scoring system
* Improved map generator
* Various game modes
* Multiplayer (Local and Online)
* Better graphics
* Sound !
* * *
### Contact ###
caupetit@student.42.fr

tmielcza@student.42.fr
* * *
### Screenshots ###
![Suneki_1.jpg](https://bitbucket.org/repo/jGL9kn/images/386165079-Suneki_1.jpg)
* * *
![Snake3.jpg](https://bitbucket.org/repo/jGL9kn/images/745557704-Snake3.jpg)